package hellopackage;

import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        Random random = new Random();
        Utilities doubler = new Utilities();
        System.out.println("Enter a number");
        int num = scan.nextInt();
        System.out.println(doubler.doubleMe(num));
        scan.close();
    }
};